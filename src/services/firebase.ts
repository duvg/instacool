import * as firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyCE56sWuOmGTnMBOUCdKzOdXbHYvLLoXk0",
    appId: "1:619512951968:web:fe19eca4efbe07df",
    authDomain: "instacool-2d94d.firebaseapp.com",
    databaseURL: "https://instacool-2d94d.firebaseio.com",
    messagingSenderId: "619512951968",
    projectId: "instacool-2d94d",
    storageBucket: "instacool-2d94d.appspot.com",
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  export const auth = firebase.auth();
  export const db = firebase.firestore();
  export const storage = firebase.storage();