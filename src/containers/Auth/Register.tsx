import * as React from 'react';
import { Link } from 'react-router-dom'; 
import Button from '../../components/Button';
import Card from '../../components/Card';
import Center from '../../components/Center';
import Container from '../../components/Container';
import Input from '../../components/Input';
import Title from '../../components/Title';
import { Field } from 'redux-form';

export default class Register extends React.Component {
    public render() {
        return (
            <Container center={true}>
                <Card>
                <Title>Registro</Title>
                <Field label='Correo' placeholder='Correo' name='email' type='email' component={ Input }/>
                <Field label='Contraseña' placeholder='Contraseña' name='password' type='password' component={ Input } />
                <Button block={'true'}>Enviar</Button>
                <Center>
                    <Link to='/'>Iniciar Sesion</Link>
                </Center>
                </Card>
            </Container>
        )
    }
}
