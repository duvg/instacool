import * as React from 'react';
import ProfileImg from '../../components/ProfileImg';
import Button from '../../components/Button';
import Card from '../../components/Card';

const styles = {
    container: {
        padding: '15px',
    },
    row: {
        display: 'flex',
        justifyContent: 'space-between',
        marginBottom: '10px'
    }
}

export default class Profile extends React.Component {
    public render() {
        return (
            <div style={styles.container}>
                <div style={styles.row}>
                    <ProfileImg />
                    <Button>Agregar</Button>
                </div>
                <div style={styles.row}>
                    <Card><img src={'https://placekitten.com/100/100'}/></Card>
                    <Card><img src={'https://placekitten.com/100/100'}/></Card>
                    <Card><img src={'https://placekitten.com/100/100'}/></Card>
                </div> 
                <div style={styles.row}>
                    <Card><img src={'https://placekitten.com/100/100'}/></Card>
                    <Card><img src={'https://placekitten.com/100/100'}/></Card>
                    <Card><img src={'https://placekitten.com/100/100'}/></Card>
                </div> 
            </div>
            
        )
    }
}
