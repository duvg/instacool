import * as React from 'react';
import { faUser, faNewspaper } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';

const styles={
    link: {
      color: '#555',
      textDecoration: 'none'  ,
    },
    navbar: {
      borderBottom: 'solid 1px #ddd',
      padding: '10px 15px'
    },
}

export default class NavBar extends React.Component {
    public render() {
        return (
            <div style={styles.navbar}>
                <Link to='/app/newsfeed' style={styles.link}>
                    <FontAwesomeIcon icon={faNewspaper}/> nstacool
                </Link>
                <div style={{float: 'right'}}>
                    <Link to='/app/profile' style={styles.link}>
                        <FontAwesomeIcon icon={faUser} /> Perfil
                    </Link>
                </div>
            </div>
        )
    }
}