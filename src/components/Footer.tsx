import * as React from 'react';
import { faThumbsUp, faRetweet } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

// Styles
const styles = {
    button: {
        cursor: 'pointer',
        flex: 1, 
        padding: '10px 15px',
        textAlign: 'center', 
    } as React.CSSProperties,
    footer: {
        backgroundColor:'#dedede', 
        display: 'flex',
        marginBottom: '-10px', 
        marginLeft: '-15px', 
        width: 'calc(100% + 30px)'
    },
}

export default class Footer extends React.Component {
    public render() {
        return (
            <div>
                <div style={styles.footer}   
                >
                    <div style={styles.button}><FontAwesomeIcon icon={faThumbsUp} /> Like</div>
                    <div style={styles.button}><FontAwesomeIcon icon={faRetweet} /> Compartir</div>
                </div>      
            </div>
        )
    }
}
