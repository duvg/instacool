import * as React from 'react';
import { InjectedFormProps, reduxForm, Field  } from 'redux-form';

import { Link } from 'react-router-dom';
import Button from './Button';
import Center from './Center';
import Input from './Input';

class LoginForm extends React.Component<InjectedFormProps> {
    public render() {
        const { handleSubmit } = this.props;
        return (
            <form onSubmit={handleSubmit}>
                <Field label='Correo' placeholder='Correo' name='email' type='email' component={Input} />
                <Field label='Contraseña' placeholder='Contraseña' name='password' type='password' component={Input} />
                <Button block={'true'}>Enviar</Button>
                <Center>
                    <Link to='/register'>Registrate</Link>
                </Center>
            </form>
        )
    }
}

export default reduxForm({
    form: 'login'
})(LoginForm);
